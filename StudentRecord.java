public class StudentRecord {
	private String name;
	private String addres;
	private int age;
	private double mathGrade;
	private double englishGrade;
	private double scienceGrade;
	private double average;
	private static int studentCount;
	/**
	* Menghasilkan nama dari siswa
	*/
	public StudentRecord(String temp) {
		this.name = temp;
	}
	public String getName(){
		return name;
	}
	/** mengubah nama siswa
	*/
	public void setName(String temp){
		name =temp;
		studentCount = studentCount+1;
	}
	// area penulisan kode lain
	/**
	*Menghitung rata-rata nilai Matematik, Bahasa Inggris, * * Ilmu Pasti
	*/
	public double getAverage(){
		double result = 0;
		result = (mathGrade+englishGrade+scienceGrade)/3;
		return result;
	}
	/**
	* Menghasilkan jumlah instance StudentRecord
	*/
	public static int  getStudentCount(){
		return studentCount;
	}
}
