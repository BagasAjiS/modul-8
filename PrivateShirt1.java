public class PrivateShirt1 {
	private int idBaju = 0; //ID default untuk baju
	private String keterangan = "-Keterangan Diperlukan-"; //default
	//Kode warna R=Merah, G=Hijau, B=Biru, U=Tidak Ditentukan
	private char kodeWarna = 'U';
	private double harga = 0.0; //harga default untuk semua barang
	private int jumlahStock = 0; //default untuk jumlah barang
	
	public char getKodeWarna(){
		return kodeWarna;
	}
	public void setKodeWarna(char kode){
		kodeWarna = kode;
	}

}
