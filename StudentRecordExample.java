public class StudentRecordExample {

	public static void main(String[] args) {
		//membuat 3 objek StudentRecord
		StudentRecord anna = new StudentRecord("Anna");
		StudentRecord beah = new StudentRecord("Beah");
		StudentRecord cris = new StudentRecord("Cris");
		
		//menampilkan nama semua siswa
		System.out.println(anna.getName());
		System.out.println(beah.getName());
		System.out.println(cris.getName());
		
		//menampilkan jumlah siswa
		System.out.println("Count = " + StudentRecord.getStudentCount());
	}
}
